package com.google.gson;

/**
 * Created by afitzgerald on 9/15/14.
 */
public class JsonElementCopier {
    public JsonElement copy(JsonElement jsonElement){
        return jsonElement.deepCopy();
    }
}
