package net.riotopsys.json_diff_sync.test;

import com.google.gson.Gson;
import net.riotopsys.json_diff_sync.BaseSyncManager;
import net.riotopsys.json_patch.JsonPatch;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by adam.fitzgerald on 7/23/14.
 */
public class BaseSyncManagerTest {

    private Gson gson = new Gson();

    @Test
    public void basicCycle(){
        BaseSyncManager bsm = new BaseSyncManager(gson.toJsonTree("{}"));

        Assert.assertEquals("[]", bsm.obtainPatch().toString());

        bsm.setLocalText(gson.toJsonTree("{\"foo\":\"bar\"}"));

        JsonPatch patch = bsm.obtainPatch();
        Assert.assertEquals("[{\"op\":\"add\",\"path\":\"/foo\",\"value\":\"bar\"}]", patch.toString());

        bsm.applyPatch(patch);
        Assert.assertEquals("[]", bsm.obtainPatch().toString());

        bsm.setLocalText(gson.toJsonTree("{\"foo\":[1,2,3,4], \"baz\":\"zip\"}"));

        patch = bsm.obtainPatch();
        Assert.assertEquals("[{\"op\":\"add\",\"path\":\"/baz\",\"value\":\"zip\"},{\"op\":\"replace\",\"path\":\"/foo\",\"value\":[1,2,3,4]}]", patch.toString());
        bsm.applyPatch(patch);

        bsm.setLocalText(gson.toJsonTree("[1,2,3,4,5]"));

        patch = bsm.obtainPatch();
        bsm.applyPatch(patch);
        Assert.assertEquals("[1,2,3,4,5]", bsm.getLocalText().toString());


    }


    @Test
    public void fullCycle(){
        List<JsonEdit> edits = new LinkedList<JsonEdit>();

        BaseSyncManager bsmA = new BaseSyncManager(gson.toJsonTree("{}"));
        BaseSyncManager bsmB = new BaseSyncManager(gson.toJsonTree("{}"));

        edits.add(new JsonEdit(gson, "{'a':'b'}", null));
        edits.add(new JsonEdit(gson, "{'a':'b', 'c':[1,2,3,4]}", null));
        edits.add(new JsonEdit(gson, "{'a':'e', 'c':[1,2,3]}", null));
        edits.add(new JsonEdit(gson, "{'a':'e', 'c':[1,2,3], 'b':'bee'}", null));
        edits.add(new JsonEdit(gson, "{'a':'e', 'c':[1,2,3], 'b':'gee'}", "{'a':'e', 'c':[1,2,3], 'b':'bee', 'foo':'bar'}"));
        edits.add(new JsonEdit(gson, "{'a':'e', 'c':[1,2,3,4], 'b':'gee'}", "{'a':'e', 'c':[1,2], 'b':'gee', 'foo':'bar'}"));
        edits.add(new JsonEdit(gson, "{'a':'e', 'c':[1,2,4,3], 'b':'gee'}", "{'a':'e', 'c':'herge', 'b':'gee'}"));
        edits.add(new JsonEdit(gson, null, "{'a':'e', 'c':'tin tin', 'b':'gee'}"));
        edits.add(new JsonEdit(gson, null, "{}"));


        for ( JsonEdit edit: edits){
            if ( edit.aSide != null ) {
                bsmA.setLocalText(edit.aSide);
            }
            if ( edit.bSide != null ) {
                bsmB.setLocalText(edit.bSide);
            }

            JsonPatch patchA = bsmA.obtainPatch();
            Assert.assertTrue(bsmB.applyPatch(patchA));
            Assert.assertEquals(bsmA.getLocalText(), bsmB.getShadowText() );
            System.out.printf("1st al: %s, as: %s, bl: %s, bs: %s \n", bsmA.getLocalText(), bsmA.getShadowText(), bsmB.getLocalText(), bsmB.getShadowText());

            JsonPatch patchB = bsmB.obtainPatch();
            Assert.assertTrue(bsmA.applyPatch(patchB));
            Assert.assertEquals(bsmB.getLocalText(), bsmA.getShadowText() );
            System.out.printf("2nd al: %s, as: %s, bl: %s, bs: %s \n", bsmA.getLocalText(), bsmA.getShadowText(), bsmB.getLocalText(), bsmB.getShadowText());

        }


    }
}
