package net.riotopsys.json_diff_sync;


import com.google.gson.JsonElement;
import net.riotopsys.json_patch.JsonPatch;

/**
 * Created by adam.fitzgerald on 7/23/14.
 */
public interface ISyncManager {

    /**
     * applies a patch from the server to both local text and shadow text
     *
     * @param patch the patch to be applied
     * @return true if applied cleanly
     */
    public boolean applyPatch(JsonPatch patch);

    /**
     * used when the system enter a non-recoverable state
     * resets both local and shadow texts will cause the loss of edits
     *
     * @param newText the value to set the texts to
     */
    public void reinitialize(JsonElement newText);

    /**
     * generate a patch between the local text and the shadow text
     *
     * @return da' patch
     */
    public JsonPatch obtainPatch();


    public JsonElement getLocalText();

    public void setLocalText( JsonElement localText );
}
