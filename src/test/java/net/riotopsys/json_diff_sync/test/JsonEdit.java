package net.riotopsys.json_diff_sync.test;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.IOException;

/**
 * Created by adam.fitzgerald on 7/25/14.
 */
public class JsonEdit {
    public JsonElement aSide, bSide;

    public JsonEdit(Gson gson, String aSide, String bSide)  {
        this.aSide = this.bSide = null;

        if ( aSide != null ) {
            this.aSide = gson.toJsonTree(aSide.replace('\'', '"'));
        }
        if ( bSide != null ) {
            this.bSide = gson.toJsonTree(bSide.replace('\'', '"'));
        }
    }

}
