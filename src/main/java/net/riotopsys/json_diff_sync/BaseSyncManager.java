package net.riotopsys.json_diff_sync;


import com.google.gson.JsonElement;
import com.google.gson.JsonElementCopier;
import net.riotopsys.json_patch.JsonPatch;
import net.riotopsys.json_patch.JsonPatchException;
import net.riotopsys.json_patch.JsonPatchFactory;

/**
 * Created by adam.fitzgerald on 7/23/14.
 */
public class BaseSyncManager implements ISyncManager {

    private JsonElement shadowText;
    private JsonElement localText;

    private JsonElementCopier copier = new JsonElementCopier();
    private JsonPatchFactory patchFactory = new JsonPatchFactory();

    public BaseSyncManager( JsonElement initialText )  {
        reinitialize(initialText);
    }

    @Override
    public void reinitialize(JsonElement newText)  {
        shadowText = copier.copy(newText);
        localText = newText;
    }

    @Override
    public boolean applyPatch(JsonPatch patch) {

        try {
            shadowText = patch.apply(shadowText);
        } catch (JsonPatchException e) {
            //must apply cleanly to shadow
            return false;
        }


        try {
            //TODO: breakout individual operations and patch them in one by one
            localText = patch.apply(localText);
        } catch (JsonPatchException e) {
            //TODO: add logging
            System.err.printf("patch: %s\n", patch.toString());
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public JsonPatch obtainPatch() {
        JsonPatch patch = patchFactory.create(shadowText, localText);

        shadowText = copier.copy(localText);

        return patch;
    }

    @Override
    public JsonElement getLocalText() {
        return localText;
    }

    @Override
    public void setLocalText(JsonElement localText) {
        this.localText = localText;
    }

    public JsonElement getShadowText() {
        return shadowText;
    }
}
